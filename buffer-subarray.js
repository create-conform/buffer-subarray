/////////////////////////////////////////////////////////////////////////////////////////////
//
// buffer-subarray
//
//    Adds subarray function to Buffer's prototype if it does not exist.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Polyfill
//
/////////////////////////////////////////////////////////////////////////////////////////////
if (typeof Buffer != "undefined" && typeof Buffer.prototype.subarray == "undefined") {
    Buffer.prototype.subarray = function (begin, end) {
        return this.slice(begin, end);
    };
}

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = Buffer;